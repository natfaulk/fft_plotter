// returns and removes highest peak
let returnSinglePeak = (_list) => {
  let max = 0
  _list.forEach((element, index) => {
    if (element > _list[max]) max = index
  })

  let left = max
  while (left - 1 >= 0) {
    if (_list[left - 1] > _list[left]) break
    left--
  }

  let right = max
  while (right + 1 < _list.length) {
    if (_list[right + 1] > _list[right]) break
    right++
  }

  // zero the peak
  for (let i = left; i <= right; i++) {
    _list[i] = 0
  }

  return max
}

let get = (_data, _nPeaks, _fs, _minFreq, _maxFreq = -1) => {
  let peaks = []
  
  // copy peaks to new list so zeroing them doesnt destroy the data from the file
  let tempList = []
  _data.forEach(element => {
    tempList.push(element.fft)
  })
  
  // sample number of cutoff frequency
  let cutoff = Math.floor((_minFreq / _fs) * tempList.length)
  let cutoffTop = tempList.length
  if (_maxFreq > _minFreq) cutoffTop = Math.floor((_maxFreq / _fs) * tempList.length)

  for (let i = 0; i < cutoff; i++) tempList[i] = 0
  for (let i = cutoffTop; i < tempList.length; i++) tempList[i] = 0

  for (let i = 0; i < _nPeaks; i++) {
    let peak = returnSinglePeak(tempList)    
    peaks.push({
      freq: Math.round(peak * _fs / tempList.length),
      value: _data[peak].fft.toFixed(2)
    })
  }
  return peaks
}

let removeFirstPeak = (_list) => {
  if (_list.length == 0) return
  
  let i = 0
  while (i + 1 < _list.length)
  {
    if (_list[i] < _list[i + 1]) break
    else _list[i] = 0
    i++
  }
}

module.exports = {
  get: get,
  returnSinglePeak: returnSinglePeak,
  removeFirstPeak: removeFirstPeak
}