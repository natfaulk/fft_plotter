#include <ESP8266WiFi.h>
#include "arduinoFFT.h"

void timer_ISR(void);
void printData(void);

const uint8_t ANALOG_PIN = A0;
const uint16_t SAMPLE_RATE = 10000;
const uint16_t NUM_SAMPLES = 1024;
const uint16_t UPDATE_RATE_ms = 250;
const uint32_t BAUD_RATE = 460800;

volatile bool dataFull = false;
volatile uint16_t data[NUM_SAMPLES];
volatile uint16_t dataI = 0;

unsigned long prevTime = 0;

// arduinoFFT FFT = arduinoFFT();
uint16_t vReal[NUM_SAMPLES];
// double vImag[NUM_SAMPLES];

void setup(void)
{
  Serial.begin(BAUD_RATE);
  Serial.println("[DEBUG] Setup started");
  timer1_isr_init();
  timer1_attachInterrupt(timer_ISR);
  timer1_enable(TIM_DIV1, TIM_EDGE, TIM_LOOP);
  timer1_write(80000000 / SAMPLE_RATE);
  Serial.println("[DEBUG] Setup done");
}

void loop(void)
{
  if (dataFull && (millis() - prevTime) > UPDATE_RATE_ms)
  {
    for (int i = 0; i < NUM_SAMPLES; i++)
    {
      vReal[i] = data[i];
      // vImag[i] = 0;
    }

    dataFull = false;


      
    // FFT.Windowing(vReal, NUM_SAMPLES, FFT_WIN_TYP_HAMMING, FFT_FORWARD);  /* Weigh data */
    // FFT.Compute(vReal, vImag, NUM_SAMPLES, FFT_FORWARD); /* Compute FFT */
    // FFT.ComplexToMagnitude(vReal, vImag, NUM_SAMPLES); /* Compute magnitudes */

    printData();
    prevTime = millis();
  }
}

void timer_ISR(void)
{
  if (!dataFull)
  {
    data[dataI++] = analogRead(ANALOG_PIN);
    if (dataI >= NUM_SAMPLES) 
    {
      dataFull = true;
      dataI = 0;
    }
  }
}

void printData(void)
{
  Serial.print("[DEBUG] ADC Data:");
  Serial.print('[');
  for (uint16_t i = 0; i < NUM_SAMPLES; i++)
  {
    Serial.print(vReal[i]);
    if (i < NUM_SAMPLES - 1) Serial.print(',');
    yield();
  }
  Serial.println(']');
}