const fs = require('fs')
const path = require('path')
const PeakDetect = require('./peakdetect')

let myApp = angular.module('myApp')
myApp.controller('dispfilesctrl', ['$scope', function($s) {
  $s.fileopen = ''
  $s.peaks = []
  $s.savedFiles = []
  $s.settings = {
    removeDC: true,
    fftAutoscale: true,
    fftHeight: 2500,
    rawAutoscale: true,
    rawHeight: 2000,
    numPeaks: 10,
    minFreq: 1500,
    maxPeakFreq: $s.$parent.settings.fs / 2
  }
  $s.imu = {
    accel: {x: 0, y: 0, z: 0},
    gyro: {x: 0, y: 0, z: 0},
    mag: {x: 0, y: 0, z: 0}
  }
  $s.getSavedFiles = () => {
    fs.readdir(path.join(__dirname, 'outputs'), (err, _files) => {
      if (err) {
        console.log(err)
        return
      }

      $s.savedFiles = _files
      $s.$apply()
    })
  }
  $s.getSavedFiles()
  
  $s.openfile = (_file) => {
    $s.fileopen = _file
    fs.readFile(path.join(__dirname, 'outputs', _file), 'utf8', (err, data) => {
      if (err) {
        console.log(err)
        return
      }

      let parseddata = {}
      
      try {
        parseddata = JSON.parse(data)
      } catch(e) {
        console.log('Error parsing file')
      }
      
      if (parseddata == {}) return
      
      $s.graph = new Mingraphing('filecanvas', {
        chartHeight: 400,
        chartMargin: 10,
        numberOfCharts: 1,
        chartWidth: 1200,
        type: 'line',
        bipolar: false,
        drawMargins: true,
        tooltip: true,
        xMax: $s.$parent.settings.fs / 2
      })
      
      $s.filedata = []
      $s.rawmax = 0
      $s.fftmax = 0
      $s.imu = parseddata.imu
      $s.avNum = 1
      if (parseddata.average) $s.avNum = parseddata.average

      if ($s.settings.removeDC) PeakDetect.removeFirstPeak(parseddata['fft'])

      parseddata['raw'].forEach(element => {
        $s.filedata.push({'raw': element})
        if (element > $s.rawmax) $s.rawmax = element
      })

      // double up fft values so only half is shown
      for (let i = 0; i < parseddata['fft'].length; i+=1)
      {
        let tempval = parseddata['fft'][Math.floor(i/2)]
        if (i < $s.filedata.length) {
          $s.filedata[i]['fft'] = tempval
        } else {
          $s.filedata.push({'fft': tempval})
        }
        if (tempval > $s.fftmax) $s.fftmax = tempval
      }

      if ($s.settings.fftAutoscale) $s.settings.fftHeight = $s.fftmax
      if ($s.settings.rawAutoscale) $s.settings.rawHeight = $s.rawmax
      $s.$apply()

      $s.graph.addData($s.filedata, 'fft', 'red', 0, $s.settings.fftHeight, 'fft')
      $s.graph.addData($s.filedata, 'raw', 'blue', 0, $s.settings.rawHeight, 'raw')
    })
  }

  $s.redrawgraph = () => {
    $s.openfile($s.fileopen)
  }

  $s.closefile = () => {
    $s.fileopen = ''
    $s.peaks = []
    $s.imu = {
      accel: {x: 0, y: 0, z: 0},
      gyro: {x: 0, y: 0, z: 0},
      mag: {x: 0, y: 0, z: 0}
    }
    $s.getSavedFiles()
  }

  $s.detectPeaks = () => {
    $s.peaks = PeakDetect.get($s.filedata, $s.settings.numPeaks, $s.$parent.settings.fs / 2, $s.settings.minFreq, $s.settings.maxPeakFreq)
  }
}])