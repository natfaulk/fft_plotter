const Point = require('./point')
const Mathjs = require('mathjs')

// const PT = 1
// const G = 1
const H = 1.838
const H2 = H * H
// const PT_G_H2 = PT * G * H2

const BORDER_WIDTH = 20
const AVERAGE_FACTOR = 0.5

const FIELD = {
  w: 250 + BORDER_WIDTH * 2,
  h: 250 + BORDER_WIDTH * 2
}
const LIGHT_R = 10
const SCALE = 2

let pos_av = new Point()

class TB {
  constructor(_x = 0, _y = 0, _fmin = 0, _fmax = 0, _gain = 0) {
    this.x = _x
    this.y = _y
    this.fmin = _fmin
    this.fmax = _fmax
    this.gain = _gain
    this.dr = 0
  }

  updateDr(power) {
    this.dr = Math.sqrt(Math.sqrt(this.gain * H2 / power) - H2)
  }

  setGain(_pr) {
    this.gain = _pr * H2
    console.log(`pr: ${_pr}, gain: ${this.gain}`)
  }
}

let TB1 = new TB(FIELD.w - BORDER_WIDTH, FIELD.h - BORDER_WIDTH, 2375, 2625, 69831.13889070002)
let TB2 = new TB(FIELD.w - BORDER_WIDTH, BORDER_WIDTH, 2625, 2875, 68129.3484711)
let TB3 = new TB(BORDER_WIDTH, BORDER_WIDTH, 2875, 3125, 52424.038932300005)
let TB4 = new TB(BORDER_WIDTH, FIELD.h - BORDER_WIDTH, 3125, 3375, 90369.24267060003)

let TBs = [TB1, TB2, TB3, TB4]

let setup = (_d, _name) => {
  _d.setup(_name, FIELD.w * SCALE, FIELD.h * SCALE)
}

let draw = (_d) => {
  _d.background('white')
  _d.stroke('green')
  
  // for (let i = 1; i < FIELD.w; i++) {
  //   _d.line(i * SCALE, 0, i * SCALE, FIELD.h * SCALE)
  // }

  // for (let i = 1; i < FIELD.h; i++) {
  //   _d.line(0, i * SCALE, FIELD.w * SCALE, i * SCALE)
  // }

  TBs.forEach((element, index) => {
    _d.stroke('green')
    _d.fill('red')
    // let pos_px = new Point((element.x + 1) * SCALE, (FIELD.h - (element.y + 1)) * SCALE)
    let pos_px = new Point((element.x) * SCALE, (FIELD.h - (element.y)) * SCALE)
    _d.ellipse(pos_px.x, pos_px.y, LIGHT_R)
    _d.text(`TB${index + 1}`, pos_px.x - 30, pos_px.y + 3)
    _d.fill('RGBA(255,255,255,0)')
    // _d.ellipse(pos_px.x, pos_px.y, element.dr * SCALE * 10 * 2)
    _d.ellipse(pos_px.x, pos_px.y, element.dr * SCALE * 2 * 100)
    console.log(element.dr)
  })

  _d.stroke('red')
  _d.fill('red')
  let pos = LLS()

  if (!Number.isNaN(pos.x) && !Number.isNaN(pos.y)) {
    pos_av.x *= AVERAGE_FACTOR
    pos_av.y *= AVERAGE_FACTOR
    pos_av.x += (1 - AVERAGE_FACTOR) * pos.x
    pos_av.y += (1 - AVERAGE_FACTOR) * pos.y
    _d.ellipse((pos_av.x) * SCALE, (FIELD.h - (pos_av.y)) * SCALE, 20)
  } else console.log('Pos was NaN')
  // _d.ellipse((pos.x + 1) * SCALE, (FIELD.h - (pos.y + 1)) * SCALE, 20)

}

let LLS = () => {
  let p = []
  let b = []
  TBs.forEach(element => {
    p.push([-2 * element.x, -2 * element.y, 1])
    b.push([sqr(element.dr * 100) - sqr(element.x) - sqr(element.y)])
    // b.push([sqr(element.dr * 10) - sqr(element.x) - sqr(element.y)])
  })

  let B = Mathjs.matrix(b)
  let A = Mathjs.matrix(p)
  let AT = Mathjs.transpose(A)
  let AT_A = Mathjs.multiply(AT, A)
  let AT_A_INV = Mathjs.inv(AT_A)
  let AT_A_INV_AT = Mathjs.multiply(AT_A_INV, AT)
  let AT_A_INV_AT_B = Mathjs.multiply(AT_A_INV_AT, B)

  // console.log(AT_A_INV_AT_B, AT_A_INV_AT_B.get([0, 0]), AT_A_INV_AT_B.get([1, 0]))
  return new Point(AT_A_INV_AT_B.get([0, 0]), AT_A_INV_AT_B.get([1, 0]))
}

let extractRadii = (_peaks) => {
  _peaks.forEach(peak => {
    let tb = getTB(peak.freq)
    if (tb !== undefined) {
      tb.updateDr(peak.value)
    }
  })
}

let getTB = (_freq) => {
  for (let i = 0; i < TBs.length; i++) {
    if (_freq >= TBs[i].fmin && _freq < TBs[i].fmax) return TBs[i]
  }
  return undefined
}

let circleIntersection = (_c1, _c2) => {
  // // (x - x1)^2 + (y - y1)^2 = r1^2
  // // (x - x2)^2 + (y - y2)^2 = r2^2

  let x1 = _c1.x
  let y1 = _c1.y
  let r1 = _c1.r
  let x0 = _c2.x
  let y0 = _c2.y
  let r0 = _c2.r

  let dx = x1 - x0
  let dy = y1 - y0

  /* Determine the straight-line distance between the centers. */
  let d = Math.hypot(dx, dy)
  
  /* Check for solvability. */
  if (d > (r0 + r1)) {
    /* no solution. circles do not intersect. */
    return []
  }
  if (d < Math.abs(r0 - r1)) {
    /* no solution. one circle is contained in the other */
    return []
  }

  /* 'point 2' is the point where the line through the circle
  * intersection points crosses the line between the circle
  * centers.  
  */

  /* Determine the distance from point 0 to point 2. */
  let a = ((r0*r0) - (r1*r1) + (d*d)) / (2.0 * d)

  /* Determine the coordinates of point 2. */
  let x2 = x0 + (dx * a/d)
  let y2 = y0 + (dy * a/d)

  /* Determine the distance from point 2 to either of the
  * intersection points.
  */
  let h = Math.sqrt((r0*r0) - (a*a))

  /* Now determine the offsets of the intersection points from
  * point 2.
  */
  let rx = -dy * (h/d)
  let ry = dx * (h/d)

  /* Determine the absolute intersection points. */
  let xi = x2 + rx
  let xi_prime = x2 - rx
  let yi = y2 + ry
  let yi_prime = y2 - ry

  return [new Point(xi, yi), new Point(xi_prime, yi_prime)]


  // // Ax + By = C
  // let A = -2 * (x1 - x2)
  // let B = -2 * (y1 - y2)
  // let C = (sqr(r1) - sqr(r2)) - (sqr(x1) - sqr(x2)) - (sqr(y1) - sqr(y2))

  // // A2x^2 + B2x + C2
  // let A2 = (1 + (sqr(A) / sqr(B)))
  // let B2 = (-2*x1 -2*(C*(A/sqr(B))) +2*y1*(A/B))
  // let C2 = sqr(x1) + sqr(y1) - sqr(r1) + sqr(C/B) - 2*y1*(C/B) 

  // let results = []
  // n_sqrt(sqr(B2) - 4*A2*C2).forEach(element => {
  //   results.push((-B2 + element) / (2*A2))
  // })
}

let sqr = (n) => {
  return Math.pow(n, 2)
}

let n_sqrt = (n) => {
  if (n < 0) return []
  if (n == 0) return [0]
  n = Math.sqrt(n)
  return [-n, n] 
}

module.exports = {
  // peakToRadius: (peak, gain) => {
  //   return Math.sqrt(Math.sqrt(gain * H2 / peak) - H2)
  // },
  draw: draw,
  setup: setup,
  extractRadii: extractRadii,
  getTB, getTB,
  circleIntersection: circleIntersection
}

