const Mingraphing = require('mingraphing')
const serialport = require('serialport')
const fft = require('jsfft')
const windowing = require('fft-windowing')
const savefiles = require('./savefiles.js')
const {app} = require('electron').remote; // for app.version
const PeakDetect = require('./peakdetect')
const Vlp = require('./vlp')
const N_ws = require('./n_ws')
const LocalIP = require('my-local-ip')
const fs = require('fs')

const RED = '#FF0000'
const GREEN = '#00FF00'
const BLUE = '#0000FF'

let myApp = angular.module('myApp', [])
myApp.controller('fftdisplay', ['$scope', '$interval', '$timeout', '$http', function($s, $interval, $timeout, $http) {
  $s.VERSION = app.getVersion()
  // Save local IP to vlp.natfaulk.com, so that the receiver board can connect to the websocket
  N_ws.postLocalIP()
  $s.ports = []
  $s.connected = false
  $s.wsSettings = {
    port: 8000,
    remoteIP: '192.168.88.12',
    thisIP: LocalIP()
  }
  $s.currentpage = 'portselect'
  $s.port = {}
  $s.dataset = []
  $s.imu = {
    accel: {x: 0, y: 0, z: 0},
    gyro: {x: 0, y: 0, z: 0},
    mag: {x: 0, y: 0, z: 0}
  }
  $s.fileOutput = {
    'raw': [],
    'fft': [],
    'imu': {},
    'average': 0
  }
  $s.highestpeak = 0
  // $s.saveNextRead = false
  // interval to tell board to begin transmit data
  $s.startinterval = null
  $s.currentFileI = 0
  $s.filePrefix = ''
  $s.settings = {
    avReadings: false,
    numToAv: 10,
    fs: 50000
  }
  $s.av = {
    fftbuff: [],
    imu: {
      accel: {x: 0, y: 0, z: 0},
      gyro: {x: 0, y: 0, z: 0},
      mag: {x: 0, y: 0, z: 0}
    },
    count: 0,
    avInProgress: false
  }

  $s.maxfreq = $s.settings.fs

  savefiles.init()

  $s.d_pos = new Mindrawingjs()
  Vlp.setup($s.d_pos, 'poscanvas')
  Vlp.draw($s.d_pos)

  $s.calibrate = {
    state: 'idle', // idle, inProgress, complete
    count: 0,
    maxCount: 10,
    freq: 0,
    value: 0
  }

  $s.calibrations = []
  fs.readFile('calibrations.json', 'utf8', function (err, data) {
    if (err)
    {
      console.log('No calibrations file, using defaults')
      $s.calibrations.push({freq: 2500, value: 10000})
      $s.calibrations.push({freq: 2750, value: 10000})
      $s.calibrations.push({freq: 3000, value: 10000})
      $s.calibrations.push({freq: 3250, value: 10000})
    } else {
      $s.calibrations = JSON.parse(data)
      console.log('Calibrations file loaded')
      $s.calibrations.forEach(element => {
        Vlp.getTB(element.freq).setGain(element.value)
      })
    }
  })

  $s.calibrateTB = (_freq) => {
    $s.calibrate.state = 'inProgress'
    $s.calibrate.count = 0
    $s.calibrate.freq = _freq
    $s.calibrate.value = 0
  }

  $s.connectPort = (_port) => {
    $s.port = new serialport(_port, {baudRate: 460800})
    $s.port.on('open', () => {
      $s.connected = true
      $s.startinterval = $interval(() => {
        $s.port.write('b\n')
      }, 5000)
      // switch to display page
      $s.currentpage = 'display'
      $s.$apply()
    })

    $s.port.on('error', () => {
      console.log('There was a serial error');
    })

    $s.dataConnect()
    // $s.graph2 = new Mingraphing('wavGraph', {
    //   chartHeight: 200,
    //   chartMargin: 10,
    //   numberOfCharts: 1,
    //   chartWidth: 1200,
    //   type: 'line',
    //   bipolar: false,
    //   drawMargins: true,
    //   tooltip: true,
    //   xMax: 25000
    // })

    // $s.graph.addData($s.dataset, 'raw', RED, 0, 2500, 'raw')

    $s.port.on('data', (data) => {
      if (angular.isDefined($s.startinterval)) {
        $interval.cancel($s.startinterval)
      }
      // console.log('Data: ' + data)
      $s.dataParse(data)
    })
  }

  serialport.list((err, _ports) => {
    if (err) console.log("Couldn't list ports")
    else {
      $s.ports = _ports
      console.log(_ports)
      $s.$apply()
    }
  })

  $s.beginReading = () => {
    if (!$s.av.avInProgress) {
      if (!$s.settings.avReadings) $s.settings.numToAv = 1 
      if ($s.settings.numToAv <= 0) $s.settings.numToAv = 1
      $s.av.avInProgress = true
      $s.fileOutput.average = 0
    }
  }

  $s.websocketBegin = () => {
    $s.dataConnect()
    N_ws.begin($s.wsSettings.port, (message) => {
      // console.log(message)
      $s.dataParse(message)
    }, (ws) => {
      ws.send('something')
      $s.dataConnect()
      $s.currentpage = 'display'
      $s.$apply()
    })
  }

  $s.ws_sendIP = () => {
    let config = {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      }
    }
    let data = `ip=${$s.wsSettings.thisIP}`
    $http.post(`http://${$s.wsSettings.remoteIP}/setremoteip`, data, config)
    .then((response) => {
      // success
      if (response.data == 'ack')
      {
        console.log('Successfully set remote ip')
        $http.post(`http://${$s.wsSettings.remoteIP}/startWs`, data, config)
        .then((response) => {
          console.log('Websocket started')
          console.log(response)
        }, (response) => {
          console.log('Failed to start websocket')
          console.log(response)
        })
      } else {
        console.log('Failed to set remote ip')
        console.log(response)
      }
    }, (response) => {
      // failure
      console.log('Failed to set remote ip')
      console.log(response)
    })
  }

  $timeout(() => {
    $s.websocketBegin()
  }, 1000)

  $s.dataConnect = () => {
    $s.graph = new Mingraphing('fftGraph', {
      chartHeight: 400,
      chartMargin: 10,
      numberOfCharts: 1,
      chartWidth: 1200,
      type: 'line',
      bipolar: false,
      drawMargins: true,
      tooltip: true,
      xMax: $s.settings.fs
    })

    $s.graph.addData($s.dataset, 'fft', RED, 0, 10000, 'fft')
    $s.graph.addData($s.dataset, 'raw', BLUE, 0, 5000, 'raw')
  }

  $s.dataParse = (data) => {
    $s.dataBuffer += data
    let tempIndex = $s.dataBuffer.indexOf('\n')
    if (tempIndex != -1) {
      let tempData = $s.dataBuffer.slice(0, tempIndex)
      if (tempData.indexOf('[DEBUG] ADC Data:') == 0 || tempData.indexOf('[INFO] ADC Data: ') == 0 || tempData.indexOf('ADC Data:') == 0)
      {
        $s.dataset = []
        $s.fileOutput.raw = []
        $s.fileOutput.fft = []
        
        let tempParsed;
        try {
          tempParsed = JSON.parse(tempData.slice(tempData.lastIndexOf('[')))
        } catch(e) {
          console.log('Error parsing file')
          $s.dataBuffer = $s.dataBuffer.slice(tempIndex + 1)
          return
        }
        
        // let tempParsed = JSON.parse(tempData.slice(tempData.lastIndexOf('[')))
        for (var i = 0; i < tempParsed.length; i++) {
          $s.dataset.push({
            'raw': tempParsed[i]
          })
          $s.fileOutput.raw.push(tempParsed[i])
        }
        let tempWindowed = windowing.hamming(tempParsed)
        let fftdata = new fft.ComplexArray(tempWindowed.length)
        fftdata.map((value, i, n) => {
          value.real = tempWindowed[i]
        })
        // console.log(fftdata)      
        fftdata.FFT()
        // console.log(fftdata)
        
        if ($s.maxfreq > $s.settings.fs) $s.maxfreq = $s.settings.fs
        if ($s.maxfreq < 0) $s.maxfreq = 0

        for (var i = 0; i < $s.dataset.length; i++) {
          let fft_abs = Math.sqrt(Math.pow(fftdata.real[i], 2) + Math.pow(fftdata.imag[i], 2))
          $s.dataset[i]['fft'] = fft_abs
          $s.fileOutput.fft.push(fft_abs)
        }

        // if ($s.av.avInProgress) {
        //   $s.currentFileI = savefiles.nextFile($s.fileOutput, $s.filePrefix)
        //   // $s.saveNextRead = false
        //   // delay else data gets corrupted with so many writes
        //   let start = Date.now()
        //   let now = start
        //   while (now - start < 500) {
        //     now = Date.now()
        //   }
        // }

        if ($s.av.avInProgress) {
          $s.fileOutput.fft.forEach((element, index) => {
            if ($s.av.count == 0) $s.av.fftbuff.push(element / $s.settings.numToAv)
            else $s.av.fftbuff[index] += element / $s.settings.numToAv
          })

          if ($s.fileOutput.imu.accel != undefined && $s.fileOutput.imu.gyro != undefined && $s.fileOutput.imu.mag != undefined) {
            $s.av.imu.accel.x += $s.fileOutput.imu.accel.x  / $s.settings.numToAv
            $s.av.imu.accel.y += $s.fileOutput.imu.accel.y  / $s.settings.numToAv
            $s.av.imu.accel.z += $s.fileOutput.imu.accel.z  / $s.settings.numToAv
            $s.av.imu.gyro.x += $s.fileOutput.imu.gyro.x  / $s.settings.numToAv
            $s.av.imu.gyro.y += $s.fileOutput.imu.gyro.y  / $s.settings.numToAv
            $s.av.imu.gyro.z += $s.fileOutput.imu.gyro.z  / $s.settings.numToAv
            $s.av.imu.mag.x += $s.fileOutput.imu.mag.x  / $s.settings.numToAv
            $s.av.imu.mag.y += $s.fileOutput.imu.mag.y  / $s.settings.numToAv
            $s.av.imu.mag.z += $s.fileOutput.imu.mag.z  / $s.settings.numToAv
          }


          $s.av.count++
          $s.fileOutput.average++

          if ($s.av.count >= $s.settings.numToAv) {
            $s.fileOutput.fft = $s.av.fftbuff
            $s.fileOutput.imu = $s.av.imu
            $s.fileOutput.version = $s.VERSION
            $s.currentFileI = savefiles.nextFile($s.fileOutput, $s.filePrefix)
            $s.av.avInProgress = false
            $s.av.count = 0
            $s.av.fftbuff = []
            $s.av.imu = {
              accel: {x: 0, y: 0, z: 0},
              gyro: {x: 0, y: 0, z: 0},
              mag: {x: 0, y: 0, z: 0}
            }
          }
        }

        if ($s.currentpage=='display') {
          let t = Math.max(Math.floor($s.dataset.length * $s.maxfreq / $s.settings.fs), 2)
          $s.dataset = $s.dataset.slice(0, t)
          $s.graph.graphs.dataset = []
          $s.graph._options.xMax = $s.maxfreq
          $s.graph.updateData($s.dataset, 'fft')
          $s.graph.updateData($s.dataset, 'raw')
          let highest = getHighestPeak($s.dataset)
          // console.log(highest)
          // console.log($s.dataset[highest])
          $s.highestpeak = highest * ($s.graph._options.xMax / $s.dataset.length)
        } else if ($s.currentpage=='position') {
          $s.lights = PeakDetect.get($s.dataset, 4, $s.settings.fs, 2000, 3500)
          Vlp.extractRadii($s.lights)
          Vlp.draw($s.d_pos)
        } else if ($s.currentpage=='settings') {
          if ($s.calibrate.state == 'inProgress') {
            let tb = Vlp.getTB($s.calibrate.freq)
            $s.calibrate.value += PeakDetect.get($s.dataset, 1, $s.settings.fs, tb.fmin, tb.fmax)[0].value / $s.calibrate.maxCount
            $s.calibrate.count++
            if ($s.calibrate.count >= $s.calibrate.maxCount)
            {
              $s.calibrate.state = 'complete'
            }
          }
          if ($s.calibrate.state == 'complete') {
            Vlp.getTB($s.calibrate.freq).setGain($s.calibrate.value)
            $s.calibrate.state = 'idle'
            
            $s.calibrations.forEach(element => {
              if (element.freq == $s.calibrate.freq) element.value = $s.calibrate.value
            })

            fs.writeFile('calibrations.json', JSON.stringify($s.calibrations), 'utf8', (err) => {
              if (err) console.log(err)
              else console.log("Calibration file saved!")
            })
          }
        }
      }

      if (tempData.indexOf('[DEBUG] IMU Data:') == 0 || tempData.indexOf('[INFO] IMU Data:') == 0 || tempData.indexOf('IMU Data:') == 0) {
        let tempParsed;
        try {
          tempParsed = JSON.parse(tempData.slice(tempData.lastIndexOf('[')))
        } catch(e) {
          console.log('Error parsing file')
          $s.dataBuffer = $s.dataBuffer.slice(tempIndex + 1)
          return
        }
        $s.imu.accel.x = tempParsed[0]
        $s.imu.accel.y = tempParsed[1]
        $s.imu.accel.z = tempParsed[2]
        $s.imu.gyro.x = tempParsed[3]
        $s.imu.gyro.y = tempParsed[4]
        $s.imu.gyro.z = tempParsed[5]
        $s.imu.mag.x = tempParsed[6]
        $s.imu.mag.y = tempParsed[7]
        $s.imu.mag.z = tempParsed[8]

        $s.fileOutput.imu = $s.imu
      }

      $s.dataBuffer = $s.dataBuffer.slice(tempIndex + 1)
      // $s.graph2.updateData($s.dataset, 'raw');
  
      // document.getElementById('highestpeakf').textContent = highest * ($s.graph._options.xMax / $s.dataset.length)
      // document.getElementById('highestpeakv').textContent = getHighestPeak(dataset)
      $s.$apply()
    }
  }
}])

// returns position of highest peak
function getHighestPeak(d) {
  let highest = 0
  let pastInitialPeak = false
  
  for (let i = 1; i < d.length; i++) {
    if (pastInitialPeak) {
      if (d[i].fft > d[highest].fft) highest = i
    } else {
      // first peak is very high cause the DC component of fft.
      // wait until goes up over previous sample
      if (d[i].fft > d[i - 1].fft) pastInitialPeak = true
      highest = i
    }
  }

  return highest
}






// function getPeakLimits(d, p) {
//   let highest = 0
//   let i = p - 1
//   while (i > 0) {
//     if (d[p] > d[p + 1]//)
//   }
// }