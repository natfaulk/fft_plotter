const WebSocket = require('ws')
const LocalIP = require('my-local-ip')
const request = require('request')

let wss;

module.exports = {
  begin: (_port, onMsgCallback, onConnectCallback) => {
    console.log('starting ws server')
    wss = new WebSocket.Server({ port: _port })

    wss.on('connection', function connection(ws) {
      ws.on('message', function incoming(message) {
        // console.log('received: %s', message)
        onMsgCallback(message)
      })
    
      // ws.send('something')
      console.log('client connected')
      onConnectCallback(ws)
    })
  },
  postLocalIP: () => {
    request.post({
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      uri: 'http://vlp.natfaulk.com/set_pc_ip',
      body: `ip=${LocalIP()}`,
      method: 'POST'
    }, (error, response, body) => {
      console.log('error:', error)
      console.log('statusCode:', response && response.statusCode)
      console.log('body:', body)
    })
  }
}