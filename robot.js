let myApp = angular.module('myApp')
const dgram = require('dgram')
let client = dgram.createSocket('udp4');

myApp.controller('robotCtrl', ['$scope', '$interval', function($s, $interval) {
  $s.robot = {
    ip: '192.168.88.11',
    remotePort: 1338,
    localPort: 1337
  }

  $s.stopped = false

  $interval(() => {
    let gamepads = navigator.getGamepads()
  
    for (let i = 0; i < gamepads.length; i++) {
      // console.log("Gamepad " + i + ":");
  
      if (gamepads[i] === null) {
          // console.log("[null]");
          continue
      }
  
      if (!gamepads[i].connected) {
          // console.log("[disconnected]");
          continue
      }

      // $s.speedL = gamepads[i].axes[2]
      if (Math.abs(gamepads[i].axes[3]) > 0.05 || Math.abs(gamepads[i].axes[2]) > 0.05)
      {
        let l = -gamepads[i].axes[3] * 100 + gamepads[i].axes[2] * 100
        if (l > 100) l = 100
        if (l < -100) l = -100

        let r = gamepads[i].axes[3] * 100 + gamepads[i].axes[2] * 100
        if (r > 100) r = 100
        if (r < -100) r = -100

        $s.leftSpeed(l)
        $s.rightSpeed(r)
        $s.stopped = false
      } else {
        if (!$s.stopped) {
          $s.stop()
          $s.stopped = true
        }
      }
      
      // $s.speedL = 
  
      // console.log("    Index: " + gamepads[i].index);
      // console.log("    ID: " + gamepads[i].id);
      // console.log("    Axes: " + gamepads[i].axes.length);
      // console.log("    Buttons: " + gamepads[i].buttons.length);
      // console.log("    Mapping: " + gamepads[i].mapping);
    }
  }, 1000 / 10)

  $s.speed = 100
  $s.speedL = 0
  $s.speedR = 0

  $s.udpconnect = () => {
  //   client.send(message, 0, message.length, 1338, '127.0.0.1', function(err, bytes) {
  //     if (err) throw err
  //     console.log('UDP message sent to ' + '127.0.0.1' +':'+ 1338)
  //     // client.close()
  // })
  }

  $s.stop = () => {
    $s.leftSpeed(0)
    $s.rightSpeed(0)
  }

  $s.forward = () => {
    $s.leftSpeed($s.speed)
    $s.rightSpeed(-$s.speed)
  }

  $s.back = () => {
    $s.leftSpeed(-$s.speed)
    $s.rightSpeed($s.speed)
  }

  $s.left = () => {
    $s.leftSpeed($s.speed)
    $s.rightSpeed($s.speed)
  }
  
  $s.right = () => {
    $s.leftSpeed(-$s.speed)
    $s.rightSpeed(-$s.speed)
  }

  $s.setLR = () => {
    $s.leftSpeed($s.speedL)
    $s.rightSpeed(-$s.speedR)
  }

  $s.leftSpeed = (_speed) => {
    let message = new Buffer(`speedL ${_speed}\r\n`)
    client.send(message, 0, message.length, $s.robot.remotePort, $s.robot.ip, (err, bytes) => {
      if (err) console.log(err)
    })
  }

  $s.rightSpeed = (_speed) => {
    let message = new Buffer(`speedR ${_speed}\r\n`)
    client.send(message, 0, message.length, $s.robot.remotePort, $s.robot.ip, (err, bytes) => {
      if (err) console.log(err)
    })
  }
}])